module.exports = {
  extends: ['airbnb-base'],
  root: true,
  env: {
    node: true,
  },
  globals: {},
  rules: {
    'max-len': ['warn'],
    'no-unused-vars': ['warn'],
    'no-console': ['error', { allow: ['warn', 'error', 'info'] }],
    'import/no-unresolved': ['error', { ignore: ['aws-sdk'] }],
  },
};
