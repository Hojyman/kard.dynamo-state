const util = require('util');
const { updateState, getState } = require('../src');

const { log } = console;

const inspect = (o) => util
  .inspect(o, {
    depth: Infinity,
    colors: true,
  // compact: 3
  });

const config = {
  tableName: 'game-state-generator-kard-tmp',

  memoryDal: false,
  // use in-memory table instead of the real DynamoDB

  allowCreateTable: true,
  // create DB table if it not exists (only when !memoryDal)

  stateIdField: 'stateId',
  eventIdField: 'actionType',
  awsLog: false, // show aws ops log
  keepLog: true, // add signals to `updates` db field.
  awsSdk: {
    region: 'eu-west-2',
  },
};

const stateId = '3918d9b3-2ac1-4f1d-845b-5bcadc72853d';

const reducers = {
  /* eslint-disable no-unused-vars, arrow-body-style */
  SET_STATE_00: async (state, { actionType, payload }, putNextSignal) => ({
    ...state,
    stateName: payload.stateName,
  }),
  SET_STATE_01: async (state, { actionType, payload }, putNextSignal) => {
    putNextSignal('SET_STATE_02', { stateName: 'STATE_02' });
    return {
      ...state,
      stateName: payload.stateName,
    };
  },
  SET_STATE_02: async (state, { actionType, payload }, putNextSignal) => {
    putNextSignal('SET_STATE_03', { stateName: 'STATE_03' });
    return {
      ...state,
      stateName: payload.stateName,
    };
  },
  SET_STATE_03: async (state, { actionType, payload }, putNextSignal) => {
    putNextSignal('SET_STATE_04', { stateName: 'STATE_04' });
    return {
      ...state,
      stateName: payload.stateName,
    };
  },
  SET_STATE_04: async (state, { actionType, payload }, putNextSignal) => {
    putNextSignal('SET_STATE_05', { stateName: 'STATE_05' });
    return {
      ...state,
      stateName: payload.stateName,
    };
  },
  SET_STATE_05: async (state, { actionType, payload }, putNextSignal) => {
    putNextSignal('SET_STATE_06', { stateName: 'STATE_06' });
    return {
      ...state,
      stateName: payload.stateName,
    };
  },
  SET_STATE_06: async (state, { actionType, payload }, putNextSignal) => {
    putNextSignal('SET_STATE_07', { stateName: 'STATE_07' });
    return {
      ...state,
      stateName: payload.stateName,
    };
  },
  SET_STATE_07: async (state, { actionType, payload }, putNextSignal) => {
    putNextSignal('SET_STATE_08', { stateName: 'STATE_08' });
    return {
      ...state,
      stateName: payload.stateName,
    };
  },
  SET_STATE_08: async (state, { actionType, payload }, putNextSignal) => {
    putNextSignal('SET_STATE_09', { stateName: 'STATE_09' });
    return {
      ...state,
      stateName: payload.stateName,
    };
  },
  SET_STATE_09: async (state, { actionType, payload }, putNextSignal) => {
    putNextSignal('SET_STATE_10', { stateName: 'STATE_10' });
    return {
      ...state,
      stateName: payload.stateName,
    };
  },
  SET_STATE_10: async (state, { actionType, payload }, putNextSignal) => {
    // putNextSignal('SET_STATE_02', {});
    return {
      ...state,
      stateName: payload.stateName,
    };
  },
};

const run = async () => {
  let currentState = await getState(stateId, { config });
  log('[getState]', inspect(currentState));

  const action = {
    actionType: 'SET_STATE_00',
    payload: {
      stateName: 'MY_STATE_00',
    },
  };

  let newState = await updateState(action, stateId, { config, reducers });
  log('[updateState]', inspect(newState));

  newState = await updateState({
    actionType: 'SET_STATE_01',
    payload: {
      stateName: 'MY_STATE_01',
    },
  }, stateId, { config, reducers });
  log('[updateState]', inspect(newState));

  currentState = await getState(stateId, { config });
  log('[getState]', inspect(currentState));
};

run();
