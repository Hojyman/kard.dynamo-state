# AWS DynamoDB state

## Usage


```js
const { getState, updateState } = require('@kard/dynamo-state');

config = {
  tableName: 'my-state-aws-dynamodb-table',
  memoryDal: false,
  allowCreateTable: true,
  stateIdField: 'stateId',
  eventIdField: 'actionType',
  awsLog: false,
  awsSdk: {
    region: 'eu-central-1',
  },
};

const stateId = '3918d9b3-2ac1-4f1d-845b-5bcadc72853d';

const currentState = await getState(stateId, { config })
console.log('[getState]', currentState);

const reducers = {
  SET_STATE: async (state, { actionType, payload }, putNextSignal) => {
    putNextSignal('SET_ANOTHER_STATE', {}) // actionType, payload
    return {
      ...state,
      stateName: payload.stateName,
    }
  },
  SET_ANOTHER_STATE: async (state, { actionType, payload }, putNextSignal) => ({
    ...state,
    stateName: payload.stateName,
  })
};

const actions = { 
	actionType: 'SET_STATE', 
	payload: {
		stateName: 'MY_STATE_NAME',
	}
}

const newState = await updateState(action, stateId, { config, reducers });
console.log('[updateState]', newState);
```

## Build

```sh
npm ci
npm run build
npx rimraf dist && npm run build
```
