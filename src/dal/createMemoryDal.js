const dbTables = {};

const cloneObj = (obj) => JSON.parse(JSON.stringify(obj));

const ddbu = {
  isTableExists(tableName) {
    return !!dbTables[tableName];
  },
  createTable(tableName) {
    dbTables[tableName] = {};
  },
  deleteTable(tableName) {
    delete dbTables[tableName];
  },
};

const createDcu = ({ stateIdField }) => ({
  getItem(tableName, keyData) {
    return cloneObj(dbTables[tableName][keyData[stateIdField]] || {});
  },
  putItem(tableName, itemData) {
    dbTables[tableName][itemData[stateIdField]] = cloneObj(itemData);
  },
  deleteItemIfExists(tableName, itemData) {
    delete dbTables[tableName][itemData[stateIdField]];
  },
});

async function createMemoryDal(config = {}) {
  const dcu = createDcu(config);

  const { tableName, dbId, stateIdField } = config;

  if (!ddbu.isTableExists(tableName)) {
    ddbu.createTable(tableName);
  }

  const dbItemToState = (dbItem) => (!dbItem.state ? [null, null] : [dbItem.state, dbItem.updates]);

  return {
    setState: async (state, updates) => dcu
      .putItem(tableName, { [stateIdField]: dbId, state, updates }),
    getState: async () => dbItemToState(dcu.getItem(tableName, { [stateIdField]: dbId })),
    deleteState: async () => dcu.deleteItemIfExists(tableName, { [stateIdField]: dbId }),
    deleteTable: async () => ddbu.deleteTable(tableName),
  };
}

module.exports = createMemoryDal;
