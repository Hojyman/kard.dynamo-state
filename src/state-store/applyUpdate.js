const { deepClone } = require('./utils');
const ErrorState = require('../errors/ErrorState');

const applyUpdate = async (update, {
  selectReducer, dal, keepLog, eventIdField,
}) => {
  let nextSignal = null;
  const putNextSignal = (eventId, payload) => {
    nextSignal = { [eventIdField]: eventId, payload };
  };

  const reduce = selectReducer(update);
  if (!reduce) {
    throw new ErrorState(`Invalid reducer: "${reduce}"`, 400);
  }

  const [state, updates] = await dal.getState();
  const newState = await reduce(state || {}, deepClone(update), putNextSignal);

  const newUpdates = updates || [];

  if (keepLog) {
    newUpdates.push(update);
  }

  await dal.setState(newState, newUpdates);
  if (nextSignal) {
    try {
      console.info('[putNextSignal]', nextSignal);

      // NOTE: (kard) https://stackoverflow.com/a/62547280
      await applyUpdate(
        nextSignal,
        {
          selectReducer, dal, keepLog, eventIdField,
        },
      );
    } catch (e) {
      console.error('[putNextSignal] ERROR: Unable apply', nextSignal, e);
      throw e;
    }
    nextSignal = null;
  }
  return newState;
};

module.exports = applyUpdate;
